-- main.lua --
javascriptding=[[
//check for browser support
if(typeof(EventSource)!=="undefined") {
    //create an object, passing it the name and location of the server side script
    var eSource = new EventSource("send_sse.php");
    //detect message receipt
    eSource.onmessage = function(event) {
        //write the received data to the page
        document.getElementById("serverData").innerHTML = event.data;
    };
}
else {
    document.getElementById("serverData").innerHTML="Whoops! Your browser doesn't receive server-sent events.";
}
]]

-- Global vars
totaltime = 0 
fwd_pin = 1
rev_pin = 2
trigger_pin = 3 

gpio.mode(fwd_pin, gpio.OUTPUT, gpio.PULLUP)
gpio.mode(rev_pin, gpio.OUTPUT, gpio.PULLUP)
gpio.mode(trigger_pin, gpio.INPUT)
gpio.write(fwd_pin, 1)
gpio.write(rev_pin, 1)

-- Config shizzle 
function createconfig()
 print("Create config")
 cfg = file.open("config.lua", "a")
 cfg:close()
end

function writeconfig(x)
 cfg = file.open("config.lua", "a")
 cfg:write(x)
 cfg:close()
end

function initialize()
 l = file.list()
 if l['config.lua'] then
   print("Open Config") 
   dofile('config.lua')
 else
   createconfig()
 end
end


-- Connect 
print('\nAll About Circuits main.lua\n')
tmr.alarm(0, 1000, 1, function()
   if wifi.sta.getip() == nil then
      print("Connecting to AP...\n")
   else
      ip, nm, gw=wifi.sta.getip()
      print("IP Info: \nIP Address: ",ip)
      print("Netmask: ",nm)
      print("Gateway Addr: ",gw,'\n')
      tmr.stop(0)
      print(gpio.read(fwd_pin))
   end
end)

function printsensor()
tmr.alarm(5, 1000, tmr.ALARM_AUTO, function()
    o = gpio.read(7)
        print ("Cur sensor value", o)
    end)
end
function livedata()
  conn:send("Live data")
end 
function calibrate()
   print("Calibrate function start")
   turnon(fwd_pin)
   y = 0
   starttime = tmr.now() 
   print("trigger_pin_reading")
   print(gpio.read(trigger_pin))
   t = gpio.read(trigger_pin)
   tmr.alarm(0, 10, tmr.ALARM_AUTO, function()
    t = gpio.read(trigger_pin)
    if t == 0 then
      print("Exiting, fwd_pin = 1")
      eindtijd = tmr.now()
      turnoff(fwd_pin)
      tmr.stop(0)
      totaltime = eindtijd - starttime
      print(totaltime)
      print("Calibrate Function end")
      w=("totaltime="..totaltime)
      writeconfig(w)
    end
   end)

end

   
function turnon(x)
    gpio.write(x, 0)
end

function turnoff(x)
    gpio.write(x, 1)
end

function openup(x)
    if gpio.read(rev_pin) == 0 then
       print ("Cowardly refusing")
       return
    end
    if gpio.read(fwd_pin) == 0 then
       print ("fwd_pin is already on, refusing")
       return
    end
    starttime = tmr.now()
    turnon(x)
    tmr.alarm(1, 1, tmr.ALARM_AUTO, function()
    curtime = tmr.now()
    runtime = curtime - starttime
    if runtime > totaltime then
        turnoff(fwd_pin)
        tmr.stop(1)
    end
    end)
end

function closedown(x)
    if gpio.read(fwd_pin) == 0 then
       print ("Cowardly refusing")
       return
    end
    if gpio.read(rev_pin) == 0 then
       print ("rev_pin is already on, refusing")
       return
    end
    starttime = tmr.now()
    turnon(x)
    tmr.alarm(1, 1, tmr.ALARM_AUTO, function()
    curtime = tmr.now()
    runtime = curtime - starttime
    if runtime > totaltime then
        turnoff(rev_pin)
        tmr.stop(1)
    end
    end)
end

function toggle(x)
    if gpio.read(x) == 1 then
                   print(x)
                   print("off, turn on")
                   gpio.write(x, 0)
                   elseif gpio.read(x) == 0 then
                   print(x)
                   print("on, turn off")
                   gpio.write(x, 1)
                   end   
     end           
 -- Start a simple http server
initialize()
srv=net.createServer(net.TCP)
srv:listen(80,function(conn)
  conn:on("receive",function(conn,payload)
    function esp_update()
     function liveupdate()
      conn:send("Here comes the live data")
     end
     
            mcu_do=string.sub(payload,postparse[2]+1,#payload)
            print(payload)
            if mcu_do == "Calibrate" then
                calibrate()
            end
            
            if mcu_do == "Open" then
               print("fwd_pin")
               print(gpio.read(fwd_pin))
               print("rev_pin")
               print(gpio.read(rev_pin))
               openup(fwd_pin)
            end
            
          if mcu_do == "Close" then
            print("fwd_pin")
            print(gpio.read(fwd_pin))
            print("rev_pin")
            print(gpio.read(rev_pin))
            closedown(rev_pin)
          end
          if mcu_do == "Debug" then
            print("fwd_pin")
            printsensor()
          end
         end
        --parse position POST value from header
        postparse={string.find(payload,"mcu_do=")}
        --If POST value exist, set LED power
        if postparse[2]~=nil then esp_update()end
        if string.match(payload, "GET /") then
         print("We got a HTTP GET")
         tgtfile = string.sub(payload,string.find(payload,"GET /") +5,string.find(payload,"HTTP/") -2 )
         if tgtfile == "refresh.ss" then
             if not conn then 
             print ("Not Connected")
             else
             conn:send("HTTP/1.1 200 OK\r\n")
             conn:send("Keep-Alive: timeout=5, max=61\r\n")
             conn:send("Connection: Keep-Alive\r\n")
             conn:send("Cache-Control: no-cache\r\n")
             conn:send("Content-Type: text/event-stream;charset=utf-8\r\n")
             tmr.alarm(6, 500, tmr.ALARM_AUTO, function()
                curtime2 = tmr.now()
                if not conn then 
                print ("Not Connected")
                tmr.stop(6)
                else
                conn:send("3b\r\n")
                conn:send("data: TIME:")
                conn:send(curtime2)
                conn:send(":CALIBTIME:")
                conn:send(totaltime)
                conn:send("\r\n")
                conn:send("\r\n\r\n\r\n0")
               end
             end)
            end
         else
         
          if tgtfile == "" then tgtfile = "index.html" end 
           f = file.open(tgtfile,"r")
           if f ~= nil then
           -- conn:send("HTTP/1.1 200 OK\r\n")
           -- conn:send("Content-Type: text/html")
           -- conn:send("Connection: Keep-Alive\r\n")
            conn:send(file.read())
            file.close()
           else
            conn:send(tgtfile.." not Found - 404 error.")
            conn:send("<html>")
          end
         end
        end
        if string.match(payload, "POST /") then
        print("We got an http post")
        f = file.open("index.html","r")
        conn:send(file.read())
        file.close()
        end
        if tgtfile ~= "refresh.ss" then
          conn:on("sent", function(conn) conn:close() end)
        
          collectgarbage()
          f = nil
          tgtfile = nil
       end
       end)
end)
